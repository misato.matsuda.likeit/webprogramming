<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="model.UserBeans" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	UserBeans ub=(UserBeans)session.getAttribute("ub");
	%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>

    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="yohaku.css" rel="stylesheet" type="text/css" />
</head>
<body>

     <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row" style="background-color: #808080;">
           <ul class="navbar-nav navbar-dark flex-row mr-auto" style="background-color: #808080;">
        <li class="nav-item active">

        </li>
      </ul>
      <ul class="navbar-nav flex-row">
        <li class="nav-item">
          <a class="nav-link" href="#">${userInfo.name }</a>
        </li>
        <li class="nav-item">
          <a class="btn btn-outline-light" href="Logout">ログアウト</a>
        </li>
      </ul>
        </nav>
    </header>

    <div class="row">
       <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <h2 class="font-weight-bold; text-warning">ユーザー新規登録</h2>
       </div>
    </div>

	 <c:if test="${errMsg != null}" >
     <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
		</c:if>


    <form action="SinkiSv" method="post">


    <p style="margin: 100px;"></p>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-3"><h5>ログインID</h5></div>
        <input type="text" value="${loginId}" style="width:250px;" name="IDus">
    </div>

    <p style="margin: 40px;"></p>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-3"><h5>パスワード</h5></div>
        <input type="password" style="width:250px;" name="pass">
    </div>

     <p style="margin: 40px;"></p>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-3"><h5>パスワード(確認)</h5></div>
        <input type="password" style="width:250px;" name="pass2">
    </div>

    <p style="margin: 40px;"></p>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-3"><h5>ユーザー名</h5></div>
        <input type="text" style="width:250px;" name="userName" value="${name}">
    </div>

    <p style="margin: 40px;"></p>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-3"><h5>生年月日</h5></div>
        <input type="date" value="${birthDate}" name="umare">
    </div>

    <p style="margin: 40px;"></p>
    <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-3"></div><input type="submit" value="登録" style="width: 150px; height: 40px;" name="touroku" class="button">
    </div>
    </form>

    <p style="margin: 40px;"></p>
    <div class="row">
        <div class="col-sm-1"></div>
            <div class="col-sm-2">
                <h6><a href="ItiranSv">戻る</a></h6>
            </div>
    </div>

</body>
</html>