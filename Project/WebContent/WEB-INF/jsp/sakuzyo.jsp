<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="model.UserBeans" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	UserBeans ub=(UserBeans)session.getAttribute("ub");
	%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>

    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link href="style.css" rel="stylesheet" type="text/css" />

</head>
<body>

     <header>
       <nav class="navbar navbar-dark navbar-expand  flex-md-row" style="background-color: #808080;">
           <ul class="navbar-nav navbar-dark flex-row mr-auto" style="background-color: #808080;">
        <li class="nav-item active">

        </li>
      </ul>
      <ul class="navbar-nav flex-row">
        <li class="nav-item">
          <a class="nav-link" href="#">${userInfo.name }</a>
        </li>
        <li class="nav-item">
          <a class="btn btn-outline-light" href="Logout">ログアウト</a>
        </li>
      </ul>
        </nav>
    </header>

     <div class="row">
       <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <h2 class="font-weight-bold; text-warning">ユーザー削除確認</h2>
       </div>
    </div>
<form action="SakuzyoSv" method="post">
    <div style="margin: 40px">
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-2"><h6>ログインID:</h6></div><h6>${onlyData.loginId}</h6>
        <input type="hidden" value="${onlyData.loginId}" name="loginId">
    </div>
        <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-5"><h6>を本当に削除してよろしいでしょうか。</h6></div>
        </div>
    </div>

    <p style="margin: 60px;"></p>
    <div class="row">
    <div class="col-sm-2"></div>
        <div class="col-sm-2"></div>
        <input type="button" value="キャンセル" onclick="location.href='ItiranSv'" style="width: 150px; height: 40px;" name="no">
        <div class="col-sm-2"></div>
        <input type="submit" value="OK" style="width: 150px; height: 40px;" name="ok" class="button">
    </div>
</form>
</body>
</html>