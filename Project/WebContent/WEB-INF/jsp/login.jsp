<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="model.UserBeans" %>
<%
	UserBeans ub=(UserBeans)session.getAttribute("ub");
	%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>

  <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
   <div class="row">
       <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <h1 class="font-weight-bold; text-warning">ログイン画面</h1>
       </div>
    </div>

    <c:if test="${errMsg != null}" >
     <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
		</c:if>

    <form class="form-signin" action="LoginSv" method="post">
   <p style="margin: 100px;"></p>
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-3"><h5>ログインID</h5></div><input type="text" style="width:200px;" name="ID">
    </div>
     <p style="margin: 30px;"></p>
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-3"><h5>パスワード</h5></div><input type="password" style="width:200px;" name="password">
    </div>
    <p style="margin: 50px;"></p>
     <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-3"></div><input type="submit" value="ログイン" style="width: 150px; height: 40px;" name="botan" class="button">
    </div>
    </form>

</body>
</html>