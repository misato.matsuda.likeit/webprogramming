<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="model.UserBeans" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	UserBeans ub=(UserBeans)session.getAttribute("ub");
	%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>

    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
     <link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <header>
         <nav class="navbar navbar-dark navbar-expand  flex-md-row" style="background-color: #808080;">
           <ul class="navbar-nav navbar-dark flex-row mr-auto" style="background-color: #019fe6;">
        <li class="nav-item active">

        </li>
      </ul>
      <ul class="navbar-nav flex-row">
        <li class="nav-item">
          <a class="nav-link" href="#">${userInfo.name }</a>
        </li>
        <li class="nav-item">
          <a class="btn btn-outline-light" href="Logout">ログアウト</a>
        </li>
      </ul>
        </nav>
    </header>

    <div class="row">
       <div class="col-sm-4"></div>
        <div class="col-sm-4">

            <h2 class="font-weight-bold; text-warning">ユーザー一覧</h2>
       </div>
    </div>

    <div class="row">
        <div class="col-sm-9"></div>
            <div class="col-sm-3">
                    <h6><a href="SinkiSv">新規登録</a></h6>
            </div>
    </div>

	  <form action="ItiranSv" method="post">
     <p style="margin: 100px;"></p>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-3"><h5>ログインID</h5></div><input type="text" style="width:250px;" name="IDus">
    </div>

     <p style="margin: 15px;"></p>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-3"><h5>ユーザー名</h5></div>
        <input type="text" style="width:250px;" name="usern">
    </div>

    <p style="margin: 5px;"></p>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-3"><h5>生年月日</h5></div>
        <input type="date"  name="toshi">
        <p>~</p>
        <input type="date" name="toshi2">
    </div>


    <p style="margin: 20px;"></p>
    <div class="row">
        <div class="col-sm-1"></div>
    <div class="col-sm-5"></div>
    <div class="col-sm-3"></div><input type="submit" value="検索" style="width: 150px; height: 40px;" name="botan" class="button">
    </div>
    <div class="border-bottom" style="padding: 5px;"></div>
    </form>

    <p style="margin: 25px;"></p>
    <table class="table table-bordered">
    <thead class="thead-light">
        <tr class="aaa">
            <th>ログインID</th><th>ユーザー名</th><th>生年月日</th><th></th>
        </tr>

        <c:forEach var="userList" items="${uList}" >
        <tr>
        <td>${userList.loginId}</td><td>${userList.name}</td>
        <td><fmt:formatDate value="${userList.birthDate}" pattern="yyyy年MM月dd日" /></td>

        <!-- ?の後の「id=${userList.id }」→idという名前で詳細のサーブレットに値を持っていく -->
            <td><a class="btn btn-primary" href="SyousaiSv?id=${userList.id }" role="button">詳細</a>
				<c:if test="${(userInfo.loginId== 'admin') or (userInfo.loginId== userList.loginId)}">
            	<a class="btn btn-success" href="KousinSv?id=${userList.id }" role="button">更新</a>
            	</c:if>
            	<c:if test="${userInfo.loginId== 'admin'}">
            	<a class="btn btn-danger" href="SakuzyoSv?id=${userList.id }" role="button">削除</a>
            	</c:if>
            </td>


        </c:forEach>
    </table>


</body>
</html>