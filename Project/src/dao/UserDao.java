package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.UserBeans;

public class UserDao {
	//ログインで探すやつ
	public UserBeans findByLoginInfo(String loginId, String re) {
		Connection conn = null;
		try {
			//データベース接続
			conn = DBmanager.getConnection();

			//sql文準備,入力したログインIDとパスがあるか
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			//select文を実行し結果表示
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, re);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			//失敗時null返す
			if (!rs.next()) {
				return null;
			}
			// 成功時カラム名指定しBeansインスタンスのフィールドにセットして返す
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new UserBeans(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//一覧全て取得
	public List<UserBeans> findAll() {
		Connection conn = null;
		//リストの箱を作る
		List<UserBeans> list = new ArrayList<UserBeans>();
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();
			// sql文を準備
			String sql = "SELECT * FROM user";
			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			// 結果をuserインスタンスに入れる
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				UserBeans user = new UserBeans(id, loginId, name, birthDate, password, createDate, updateDate);
				//作ったリスト「list」に情報を入れる
				list.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		//トライキャッチ外から作成したリストを返してあげる
		return list;
	}
	//新規登録のやつ
	public void newData(String loginId, String re, String name, String birthDate) {
		Connection conn = null;
		try {
			//データベース接続
			conn = DBmanager.getConnection();

			//insert文準備
			String sql = "INSERT INTO user (login_id , password , name , birth_date , create_date , update_date) VALUES (? , ? , ? , ?,now(),now())";
			//insert文を実行し結果を取得
			PreparedStatement sm = conn.prepareStatement(sql);
			sm.setString(1, loginId);
			sm.setString(2, re);
			sm.setString(3, name);
			sm.setString(4, birthDate);
			int result = sm.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}
		//更新のやつ
	public void update(String loginId,String re,String name,String birthDate) {
		Connection conn = null;
		try {
			//データベース接続
			conn = DBmanager.getConnection();

			//insert文準備
			String sql = "UPDATE user SET password=? , name=? , birth_date=? WHERE login_id=?";
			//update文を実行
			PreparedStatement sm = conn.prepareStatement(sql);
			sm.setString(1, re);
			sm.setString(2,name);
			sm.setString(3,birthDate);
			sm.setString(4,loginId);
			int result = sm.executeUpdate();

	}catch (SQLException e) {
		e.printStackTrace();

	} finally {
		// データベース切断

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();

			}
		}
	}

}
	//更新でパスワードが無いパターン
	public void update2(String loginId,String name,String birthDate) {
		Connection conn = null;
		try {
			//データベース接続
			conn = DBmanager.getConnection();

			//insert文準備
			String sql = "UPDATE user SET name=? , birth_date=? WHERE login_id=?";
			//update文を実行
			PreparedStatement sm = conn.prepareStatement(sql);
			sm.setString(1,name);
			sm.setString(2,birthDate);
			sm.setString(3,loginId);
			int result = sm.executeUpdate();

	}catch (SQLException e) {
		e.printStackTrace();

	} finally {
		// データベース切断

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();

			}
		}
	}

}

	//詳細の個人のデータ
	public UserBeans onlyData(String id) {
		Connection conn = null;
		try {
			//データベース接続
			conn = DBmanager.getConnection();

			//sql文準備
			String sql = "SELECT * FROM user WHERE id=?";
			//実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			//結果内容
			if (!rs.next()) {
				return null;
			}
			// 成功時カラム名指定しBeansインスタンスのフィールドにセットして返す
			int idData = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			return new UserBeans(idData, loginId, name, birthDate, password, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			// データベース切断

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

					return null;
				}

			}
		}

	}

	//削除のやつ
	public void sakuzyo(String loginId) {
		Connection conn = null;
		try {
			//データベース接続
			conn = DBmanager.getConnection();

			//sql文準備
			String sql = "DELETE FROM user WHERE login_id=?";
			//実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			int rs = pStmt.executeUpdate();



	}catch (SQLException e) {
		e.printStackTrace();

	} finally {
		// データベース切断

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();

			}
		}
	}
}
	//ログインIDが他にないか調べるやつ
	public String login(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();
			// sql文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? ";
			//実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,loginId);
			ResultSet rs = pStmt.executeQuery();

			//結果同じIDが無かった場合nullを返す
			if (!rs.next()) {
				return null;
			}

		// 同じIDがあった場合nullじゃない何かを返す。なんでもいい
		String dupli= rs.getString("login_id");
		return new String(dupli);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

		//パスワードの暗号化
			public String md5(String password) {
				try {
					//暗号にしたいやつ
					String source = password;
					//文字変換
					Charset charset = StandardCharsets.UTF_8;
					//何ハッシュにするか
					String algorithm = "MD5";
					//バイトで作りストリングに直す作業
					byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
					String result = DatatypeConverter.printHexBinary(bytes);

					return result;

					} catch (Exception e) {
						e.printStackTrace();
						return null;
					}
						}

			//検索システム
			public List<UserBeans>search(String id,String name,String bd1,String bd2) {
				Connection conn = null;
				List<UserBeans> list = new ArrayList<UserBeans>();
				try {
					// データベースへ接続
					conn = DBmanager.getConnection();

					// sql文を準備,まず管理者はnot likeで除外する
					String sql ="SELECT * FROM user WHERE login_id not like 'admin'";
					//if文で条件を追加していく。idがnullで無いとき？だと使いにくいからインスタンス使う
							if(!id.equals("")) {
								sql += " and login_id = '"+ id +"'";
							}
							if(!name.equals("")) {
								sql += " and name LIKE '%"+ name +"%'";
							}
							if(!bd1.equals("")&&!bd2.equals("")) {
								sql += " and birth_date BETWEEN '"+ bd1 +"' AND '"+ bd2 +"'";
							}

							// SELECTを実行し、結果表を取得
							Statement stmt = conn.createStatement();
							ResultSet rs = stmt.executeQuery(sql);

					//
					while(rs.next()) {
						int i = rs.getInt("id");
						String loginId = rs.getString("login_id");
						String na = rs.getString("name");
						Date birthDate = rs.getDate("birth_date");
						String password = rs.getString("password");
						String createDate = rs.getString("create_date");
						String updateDate = rs.getString("update_date");
						UserBeans user = new UserBeans(i, loginId, na, birthDate, password, createDate, updateDate);
						//リストに入れる作業
						list.add(user);
					}
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				} finally {
					// データベース切断
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
							return null;
						}
					}
				}
				return list;
			}
}






