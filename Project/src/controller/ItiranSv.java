package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.UserBeans;

/**
 * Servlet implementation class ItiranSv
 */
@WebServlet("/ItiranSv")
public class ItiranSv extends HttpServlet {
	private static final long serialVersionUID = 1L;


    /**
     *
     *
     * @see HttpServlet#HttpServlet()
     */
    public ItiranSv() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		List<UserBeans> userList = userDao.findAll();

		// リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("uList", userList);

		// 一覧jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itiran.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			// リクエストパラメータの文字化け防止
				request.setCharacterEncoding("UTF-8");

			// リクエストパラメータの入力項目を取得
				String id = request.getParameter("IDus");
				String name = request.getParameter("usern");
				String bd1 = request.getParameter("toshi");
				String bd2 = request.getParameter("toshi2");

				UserDao userDao = new UserDao();
				List<UserBeans> result = userDao.search(id, name, bd1, bd2);

				// リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("uList", result);

				// 一覧jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itiran.jsp");
				dispatcher.forward(request, response);

	}

}
