package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class SinkiSv
 */
@WebServlet("/SinkiSv")
public class SinkiSv extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SinkiSv() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sinki.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// リクエストパラメータの文字化け防止
				request.setCharacterEncoding("UTF-8");
			// リクエストパラメータの入力項目を取得
				String loginId = request.getParameter("IDus");
				String password = request.getParameter("pass");
				String password2 = request.getParameter("pass2");
				String name = request.getParameter("userName");
				String birthDate = request.getParameter("umare");

				//daoを呼び出し
				UserDao userDao = new UserDao();
				//id重複確認
				String id=userDao.login(loginId);

				//エラー処理
				if(id!=null) {
					request.setAttribute("errMsg", "入力された内容は正しくありません。");
					request.setAttribute("loginId",loginId);
					request.setAttribute("name",name);
					request.setAttribute("birthDate",birthDate);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sinki.jsp");
					dispatcher.forward(request, response);
					return;
			}
				//パスワード1とパスワード2が等しくない場合
				if(!password.equals(password2)) {
					request.setAttribute("errMsg", "入力された内容は正しくありません。");
					request.setAttribute("loginId",loginId);
					request.setAttribute("name",name);
					request.setAttribute("birthDate",birthDate);

					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sinki.jsp");
					dispatcher.forward(request, response);
					return;
				}//入力項目に未入力がある場合
				if(loginId.equals("")||password.equals("")||password2.equals("")||name.equals("")||birthDate.equals("")) {
					request.setAttribute("errMsg", "入力された内容は正しくありません。");
					request.setAttribute("loginId",loginId);
					request.setAttribute("name",name);
					request.setAttribute("birthDate",birthDate);

					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sinki.jsp");
					dispatcher.forward(request, response);
					return;
				}
				String re=userDao.md5(password);
				//登録
				userDao.newData(loginId, re, name, birthDate);
				// ユーザ一覧のサーブレットにリダイレクト
				response.sendRedirect("ItiranSv");


	}
}



