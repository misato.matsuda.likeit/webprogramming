package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.UserBeans;

/**
 * Servlet implementation class KousinSv
 */
@WebServlet("/KousinSv")
public class KousinSv extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public KousinSv() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//リクエスト文字化け防止
				request.setCharacterEncoding("UTF-8");
				//idの値を取得

				String id = request.getParameter("id");
				//daoメソッド実行
				UserDao userDao = new UserDao();
				UserBeans onlyData=userDao.onlyData(id);

				request.setAttribute("onlyData", onlyData);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/kousin.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字化け防止
				request.setCharacterEncoding("UTF-8");
				// リクエストパラメータの入力項目を取得
				String id = request.getParameter("id");
				String loginId = request.getParameter("loginId");
				String password = request.getParameter("pass3");
				String password2 = request.getParameter("pass4");
				String name = request.getParameter("usern2");
				String birthDate = request.getParameter("umare2");

				//daoを呼び出し
				UserDao userDao = new UserDao();
				UserBeans reDate=userDao.onlyData(id);

				//エラー
				//パスワードの違い
				if(!password.equals(password2)) {
					request.setAttribute("errMsg", "入力された内容は正しくありません。");
					request.setAttribute("onlyData", reDate);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/kousin.jsp");
					dispatcher.forward(request, response);
					return;

				}//入力項目に未入力がある場合
				if(name.equals("")||birthDate.equals("")) {
					request.setAttribute("errMsg", "入力された内容は正しくありません。");
					request.setAttribute("onlyData", reDate);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/kousin.jsp");
					dispatcher.forward(request, response);
					return;
				}//パスワード空白時
				if(password.equals("")&&password2.equals("")) {
					userDao.update2(loginId, name, birthDate);
					response.sendRedirect("ItiranSv");
					return;
				}
				String re=userDao.md5(password);
				//更新
				userDao.update(loginId,re,name,birthDate);

				// ユーザ一覧のサーブレットにリダイレクト
				response.sendRedirect("ItiranSv");




	}

}
