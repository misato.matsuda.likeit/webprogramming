package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.UserBeans;

/**
 * Servlet implementation class SakuzyoSv
 */
@WebServlet("/SakuzyoSv")
public class SakuzyoSv extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SakuzyoSv() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//リクエスト文字化け防止
		request.setCharacterEncoding("UTF-8");
		//idの値を取得
		String id = request.getParameter("id");
		//daoメソッド実行
		UserDao userDao = new UserDao();
		UserBeans onlyData=userDao.onlyData(id);

		request.setAttribute("onlyData", onlyData);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sakuzyo.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字化け防止
				request.setCharacterEncoding("UTF-8");
				
				//idの値を取得
				String loginId = request.getParameter("loginId");
				//daoメソッド実行
				UserDao userDao = new UserDao();
				userDao.sakuzyo(loginId);
				
				// ユーザ一覧のサーブレットにリダイレクト
				response.sendRedirect("ItiranSv");

	}

}
